# SSO and Authorization Service docker images

This repository contains base images for some of our docker deployments.
The inner folders contain `Dockerfile`s which build the specific images.

## Images

* [`authzsvc-api-base-al9-native`](authzsvc-api-base-al9-native): Base run image for AuthorizationServiceApi, AuthorizationServiceJobRunner
* [`authzsvc-api-base-al9`](authzsvc-api-base-al9)
* [`dotnet-sdk-60-bullseye`](dotnet-sdk-60-bullseye): Base builder image for .NET core 6 code
* [`dotnet-sdk-80-bookworm`](dotnet-sdk-80-bookworm): Base builder image for .NET core 8 & .NET Framework code
* [`dotnet-sdk-ci-60`](dotnet-sdk-ci-60): Base builder image for .NET core 6 and .NET Framework code
* [`flume`](flume): Log forwarding side car (to be deleted by MALTIAM-3837)
* [`integration-tests-base`](integration-tests-base): Selenium image, customised for our integration tests
* [`keycloak-base-rpm`](keycloak-base-rpm): Keycloak vanilla version, with additional RPMs
* [`keycloak-cern-k8s`](keycloak-cern-k8s): Keycloak CERN build, for production deployment
* [`node-base-10`](node-base-10)
* [`node-base-14`](node-base-14)
* [`node-base-18`](node-base-18)
* [`python-base-311`](python-base-311)
* [`python-base-38`](python-base-38): Used by account-management
* [`python-base-39`](python-base-39)
