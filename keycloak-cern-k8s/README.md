# Keycloak CERN Kubernetes Build

**This image is meant to run on Kubernetes.** All customization are done at level of Keycloak Operator, as described in the [admin docs](https://auth-admin.docs.cern.ch/keycloak/kubernetes_production_deployment/).

For testing purposes, it's also possible to run it locally, as described below.

## Local instance with Podman or Docker

ℹ _Examples below use `podman` CLI. `docker` CLI  could be used too, with the same syntax. However, Docker Desktop must not be used at CERN - please use [Podman Desktop](https://podman-desktop.io/) instead._

Build the image with CERN configuration:

```bash
podman build -t keycloak24-cern .
```

Put the following files ([more details](https://auth-admin.docs.cern.ch/keycloak/kubernetes_production_deployment/#files)) in `local-config` directory that will be mounted into `/etc/keycloak`:

* `keycloak-dev.env` (mandatory) - environment variables with the database configuration.
* `ebean.properties` (mandatory, for CERN SPI [`keycloak-cern-providers`](https://auth-admin.docs.cern.ch/keycloak/extensions/#cern-specific-providers-keycloak-cern-providers) to work)
* `rsa-local.keys` (optional)
* `keycloakbind.keytab` (optional, for Kerberos authentication)

Run the container:
```bash
podman run -p 8080:8080 --env-file local-config/keycloak24-dev.env \
  -v "$(pwd)"/local-config/ebean.properties:/etc/keycloak/ebean.properties \
  -v "$(pwd)"/local-config/rsa-local.keys:/etc/keycloak/rsa.keys \
  --name keycloak-dev --rm keycloak24-cern start-dev
```

... and access it at <http://localhost:8080/auth/admin> (admin console) or <http://localhost:8080/auth/realms/cern/account> => "Sign in" for user login.

## Keycloak upgrade notes

Please see [separate notes for Keycloak upgrades](upgrade-notes.md).

## Files

* [`krb5.conf`](krb5.conf) - comes from [Kerberos at CERN](https://linux.web.cern.ch/docs/kerberos-access/#client-configuration-kerberos) documentation -> [`krb5.conf`](https://linux.web.cern.ch/docs/krb5.conf), and is adapted by removing references to non-CERN domains.
* [`cacerts`](cacerts) - for Java applications (including Keycloak), we take `cacerts` from Java distribution and add CERN certificates on top of that (NB: we don't take this file from `ca-certificates` RPM package, as used e.g. on lxplus/lxadm).
* [`cerntruststore`](cerntruststore) - a Java KeyStore with CERN Grid CA certificate. To see its content, run `keytool -v -list -storepass changeme -keystore cerntruststore`. This file is referenced in `cache-ispn.xml` files (Infinispan configuration) in [`authzsvc-users`](https://gitlab.cern.ch/jeedy/gitops/authzsvc-users) repository.

### Updating `cacerts`

```bash
# get the updated file from Antoni Nappi or Artur Wiecek
# https://linuxsoft.cern.ch/internal/repos/jeedy9al-stable/x86_64/os/Packages/j/
# one of JEEDY RPMs, corresponding the Java version used by Keycloak

# see the content of the file
keytool -list -storepass changeit -keystore cacerts

# add it to "cacerts-files" directory
cp cacerts cacerts-files/cacerts-CERN-`date "+%Y%m%d"`

# export all cacert-files
cd cacerts-files

ls -1 | grep -v "\.txt" | while read f; do
  echo $f
  keytool -list -storepass changeit -keystore $f  | \
    tr '\n' '\r' | sed -e "s#\rCertificate fingerprint ##g" | tr '\r' '\n' | \
    sort > $f.txt.sorted
  keytool -v -list -storepass changeit -keystore $f | \
    grep --color=never "^Keystore\|keystore\|^Alias\|^Owner\|^Valid\| SHA256:" \
    > $f.txt
done
```

**Notes** (from January 2024)

* The difference between `cacerts-keycloak-20.0.5` and `cacerts-CERN-20230609` is just additional 6 CERN certificates:
```bash
diff cacerts-keycloak-20.0.5.txt cacerts-CERN-20230609.txt

< Your keystore contains 136 entries
---
> Your keystore contains 142 entries
[.. 6 CERN certificates ..]
```
* Keycloak 20 and 21 share the same cacerts content. Same for Keycloak 23 and 24:
```bash
md5 cacerts-keycloak-??.?.?.txt

MD5 (cacerts-keycloak-20.0.5.txt) = 3528f77e15c76fb3da2b20268ec7b888
MD5 (cacerts-keycloak-21.1.2.txt) = 3528f77e15c76fb3da2b20268ec7b888
MD5 (cacerts-keycloak-22.0.5.txt) = 8c5e57994ffca9fd8c9e17edd52cbe23
MD5 (cacerts-keycloak-23.0.7.txt) = 13850744c38dca421ba21bc7060d57aa
MD5 (cacerts-keycloak-24.0.3.txt) = 13850744c38dca421ba21bc7060d57aa
```
* The difference between Keycloak 20/21 and 23/24 cacerts is several added certificates and a few removed:
```bash
diff cacerts-keycloak-21.1.2.txt cacerts-keycloak-23.0.7.txt

< Your keystore contains 136 entries
---
> Your keystore contains 142 entries
[..]
```
           
* New `cacerts` file:
```bash
# as suggested by Artur
cd /tmp

curl https://linuxsoft.cern.ch/internal/repos/jeedy9al-stable/x86_64/os/Packages/j/jeedy-adoptopenjdk-11-generic-11.0.19_7-6.al9.cern.x86_64.rpm | \
rpm2cpio | \
cpio -icdum ./usr/adoptopenjdk/jdk-adoptopenjdk-11/lib/security/cacerts

ls -al usr/adoptopenjdk/jdk-adoptopenjdk-11/lib/security/cacerts

# checking other available versions
for rpm in 11.0.19_7-11.0.19_7-6 11.0.21_9-11.0.21_9-0 generic-11.0.19_7-6 generic-11.0.21_9-0; do
  url=https://linuxsoft.cern.ch/internal/repos/jeedy9al-stable/x86_64/os/Packages/j/jeedy-adoptopenjdk-11-$rpm.al9.cern.x86_64.rpm

  curl $url | \
  rpm2cpio | \
  cpio -icdum ./usr/adoptopenjdk/jdk-adoptopenjdk-11/lib/security/cacerts

  mv usr/adoptopenjdk/jdk-adoptopenjdk-11/lib/security/cacerts cacerts-$rpm
done

md5sum cacerts-*
```
