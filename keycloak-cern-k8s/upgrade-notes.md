# Keycloak upgrade notes

## Keycloak 20 to 24

Files:

* `cacerts` updated to the new one from Artur (for Java 17)
* `krb5.conf` checked, no updates upstream
* `cerntruststore` checked, the certificate inside is still valid and expires in 2032, no need to update

External SPIs:

* `keycloak-metrics-spi` - Decided to use Metrics version [5.0.0](https://github.com/aerogear/keycloak-metrics-spi/releases/tag/5.0.0), as it is for Keycloak 23 and Java 17, and it also seems to [support Keycloak 24](https://github.com/aerogear/keycloak-metrics-spi/issues/198). The next version, [6.0.0](https://github.com/aerogear/keycloak-metrics-spi/releases/tag/6.0.0), is for Keycloak 25, and Java 21.
* `keycloak-health-checks` - It seems that this extension is indeed no longer maintained, and doesn't support Keycloak versions above 20: <https://github.com/thomasdarimont/keycloak-health-checks/issues/35>. More details at [MALTIAM-3673](https://its.cern.ch/jira/browse/MALTIAM-3673)

[Features](https://www.keycloak.org/server/features):

* `declarative-user-profile` feature is [fully supported and enabled by default](https://www.keycloak.org/2024/03/keycloak-2400-released) in Keycloak 24 - so no need to explicitly enable it
* `admin-fine-grained-authz` feature is still [preview and disabled by default](https://www.keycloak.org/docs/24.0.5/server_admin/index.html#_fine_grain_permissions) in Keycloak 24
* `token-exchange` is still [preview and disabled by default](https://www.keycloak.org/server/features#_preview_features) in Keycloak 24
