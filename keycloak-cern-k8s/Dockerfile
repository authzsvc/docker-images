# Reference: https://www.keycloak.org/operator/customizing-keycloak
FROM quay.io/keycloak/keycloak:24.0.5

WORKDIR /opt/keycloak/providers

ENV MAVEN_URL='https://cern-nexus.web.cern.ch/repository/maven-releases'

ENV VER_PROVIDERS='24.1.8'
ENV VER_CAPTCHA='24.0.0'
ENV VER_STEPUP='24.0.0'
ENV VER_THEME='24.1.4'
ENV VER_METRICS='5.0.0'
ENV VER_LDAP_IMPORTER='24.0.0'

ADD --chown=keycloak:root --chmod=644 $MAVEN_URL/ch/cern/keycloak-cern-providers/$VER_PROVIDERS/keycloak-cern-providers-$VER_PROVIDERS.jar ./
ADD --chown=keycloak:root --chmod=644 $MAVEN_URL/ch/cern/keycloak-cerncaptcha/$VER_CAPTCHA/keycloak-cerncaptcha-$VER_CAPTCHA.jar ./
ADD --chown=keycloak:root --chmod=644 $MAVEN_URL/ch/cern/cert/keycloak/keycloak-stepup-rest/$VER_STEPUP/keycloak-stepup-rest-$VER_STEPUP.jar ./
ADD --chown=keycloak:root --chmod=644 $MAVEN_URL/ch/cern/keycloak-cern-theme/$VER_THEME/keycloak-cern-theme-$VER_THEME.jar ./
ADD --chown=keycloak:root --chmod=644 $MAVEN_URL/ch/cern/keycloak-cern-ldap-migration/$VER_LDAP_IMPORTER/keycloak-cern-ldap-migration-$VER_LDAP_IMPORTER.jar ./
ADD --chown=keycloak:root --chmod=644 $MAVEN_URL/org/jboss/aerogear/keycloak-metrics-spi/$VER_METRICS/keycloak-metrics-spi-$VER_METRICS.jar ./

WORKDIR /

COPY --chown=keycloak:root --chmod=644 cacerts /etc/pki/ca-trust/extracted/java/cacerts
COPY --chown=keycloak:root --chmod=644 krb5.conf /etc/krb5.conf
COPY --chown=keycloak:root --chmod=644 cerntruststore /etc/keycloak/cerntruststore

# The following environmental variables with Kecyloak and Quarkus settings
# are now set and managed in Keycloak configuration repository
# (https://gitlab.cern.ch/jeedy/gitops/authzsvc-users).
# They are kept here in the comments below as a reference,
# in case this Dockerfile is used to instantiate Keycloak directly (not via ArgoCD),
# for example for SPI testing.
#
# ENV KC_HTTP_RELATIVE_PATH=/auth
# ENV KC_METRICS_ENABLED=true
# ENV KC_HEALTH_ENABLED=true
# ENV KC_LOG_LEVEL=INFO,org.keycloak.events:DEBUG
# ENV KC_FEATURES=admin-fine-grained-authz,token-exchange,scripts,declarative-user-profile
#
# ENV KC_DB=mysql
# ENV KC_DB_POOL_MAX_SIZE=50
# ENV KC_DB_USERNAME=...
# ENV KC_DB_PASSWORD=...
# ENV KC_DB_URL=...
#
# ENV CERN_EBEAN_CFG_PATH=/etc/keycloak/ebean.properties
# ENV CERN_COMPROMISED_PASSWORD_HIKARI_CFG_PATH=/etc/keycloak/compromised.password.hikari.properties
# ENV CERN_AUTHZSVC_HIKARI_CFG_PATH=/etc/keycloak/authzsvc.hikari.properties
#
# ENV QUARKUS_LOG_FILE_JSON_DATE_FORMAT='yyyy-MM-dd''T''HH:mm:ss.SSSXXX'
# ENV QUARKUS_LOG_FILE_ROTATION_MAX_BACKUP_INDEX='3'
# ENV QUARKUS_LOG_FILE_ROTATION_MAX_FILE_SIZE=256M
#
# ENV QUARKUS_HTTP_ACCESS_LOG_ENABLED=true
# ENV QUARKUS_HTTP_ACCESS_LOG_PATTERN="%m %R (ClientIP:%a Referer:%{i,Referer} User-agent:%{i,User-Agent})"
# ENV QUARKUS_HTTP_ACCESS_LOG_EXCLUDE_PATTERN="/auth/health/ready|/auth/realms/cern/metrics|/auth/metrics|/auth/health/live|/auth/realms/cern/health/check|/auth/admin|/auth/admin/master/console/|^/auth/resources.*"
