# Keycloak base version, with additional RPMs

This is a vanilla version of Keycloak, with no CERN additions and customisations - but with a possibility of [installing additional RPMs](https://www.keycloak.org/server/containers#_installing_additional_rpm_packages). This image can be instantiated for example to:

* see the files originially distributed with Keycloak (e.g. `cacerts`)
* run `kcadm.sh` ([Keycloak admin CLI](https://www.keycloak.org/docs/latest/server_admin/#admin-cli)) 
* run arbitrary tools (e.g. `jq`), installed from RPMs, directly in the container
  
Before building the image, put the desired version of Keycloak (see [available tags](https://quay.io/repository/keycloak/keycloak?tab=tags)) into `Dockerfile`.

```bash
# build image
podman build -t keycloak-base .

# -------------------------------------------
# run and detach a short-lived container
podman run --entrypoint='["sleep", "1000"]' --rm -d --name keycloak-local keycloak-base

# list running containers
podman container list

# run terminal
podman exec -it keycloak-local /bin/bash

# execute a single command
podman exec keycloak-local cat /opt/keycloak/version.txt
podman exec keycloak-local java --version
podman exec keycloak-local ls -1 /opt/keycloak/lib/lib/main | grep org.infinispan.infinispan
podman exec keycloak-local ls -l /etc/pki/ca-trust/extracted/java/cacerts

# copy a file from the container
podman cp keycloak-local:/etc/pki/ca-trust/extracted/java/cacerts cacerts-from-keycloak

# run kcadm.sh, pointing to https://keycloak-qa.cern.ch
# (Keycloak CLI docs: https://www.keycloak.org/docs/latest/server_admin/#admin-cli)
podman exec -ti keycloak-local \
  /opt/keycloak/bin/kcadm.sh config credentials \
  --server https://keycloak-qa.cern.ch/auth --realm master --user admin
podman exec keycloak-local \
  /opt/keycloak/bin/kcadm.sh get realms | \
  jq -r ".[] | .realm"

# kill the container (it will get removed automatically)
podman container kill keycloak-local

# -------------------------------------------
# alternatively, run Keycloak in a container and forward port 8080
podman run -p 8080:8080 --rm --name keycloak-local keycloak-base

# * and connect to http://localhost:8080/admin
# * once done, kill it with Ctrl-C
```

## Keycloak, Java and Infinispan versions

| Keycloak | Java                             | Infinispan |
| -------- | -------------------------------- | ---------- |
| 20.0.5   | `openjdk 11.0.18 2023-01-17 LTS` | 13.0.10    |
| 21.1.2   | `openjdk 17.0.7 2023-04-18 LTS`  | 14.0.9     |
| 22.0.5   | `openjdk 17.0.9 2023-10-17 LTS`  | 14.0.17    |
| 23.0.7   | `openjdk 17.0.10 2024-01-16 LTS` | 14.0.21    |
| 24.0.5   | `openjdk 17.0.11 2024-04-16 LTS` | 14.0.27    |
| 25.0.0   | `openjdk 21.0.3 2024-04-16 LTS`  | 15.0.4     |
