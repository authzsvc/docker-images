#!/bin/bash

FLUME_CONF_DIR=/opt/flume-config
FLUME_AGENT_DIR=/opt/flume-agents

[[ -d "${FLUME_AGENT_DIR}"  ]]  || { echo "Flume config file not mounted in /opt/flume-agents";  exit 1; }
[[ -z "${FLUME_AGENT_NAME}" ]] && { echo "FLUME_AGENT_NAME required"; exit 1; }
[[ -z "${LOG_LEVEL}" ]] && LOG_LEVEL="INFO"


echo "Starting flume agent : ${FLUME_AGENT_NAME}"

flume-ng agent \
  -c ${FLUME_CONF_DIR} \
  -f ${FLUME_AGENT_DIR}/flume.conf \
  -n ${FLUME_AGENT_NAME} \
  -Dflume.root.logger=${LOG_LEVEL},console \
  -Dorg.apache.flume.log.printconfig=true \
  -Dorg.apache.flume.log.rawdata=true \
  $*